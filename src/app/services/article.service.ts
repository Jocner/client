import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../models/article';
import { global } from './global';

@Injectable()
export class ArticleService{
    public url: string;

    constructor(private _http: HttpClient){
        this.url = global.url;
    }

    prueba(){
        return "Hola Mundo desde el Servicio de Angular";
    }

    getArticle():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        
        return this._http.get(this.url+'articles', {headers: headers});
    }

    deleteArticle(id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');


        return this._http.delete(this.url+'delete-article/'+id , {headers: headers});                               
    }

}