export class Article{
    constructor(
       public _id: string,
       public name: string,
       public description: string,
       public time: Date
    ){}
}