import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../src/app/models/article'
import { global } from '../src/app/services/global';


@Injectable()
export class ArticleService{
    public url: string;
 
    constructor(private _http: HttpClient){
         this.url = global.url;
    }

 

    register(articles): Observable<any>{
        //Convertir el objeto del usuario a un Json string
        let params = JSON.stringify(articles);


        //Definir las cabeceras
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        //Hacer peticion ajax
        return this._http.post(this.url+'save-article', params, {headers: headers});

    }


    getArticle():Observable<any>{
        return this._http.get(this.url+'articles');
    }

    delete(id): Observable<any>{
        

        //Definir las cabeceras
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        //Hacer peticion ajax
        return this._http.delete(this.url+'delete-article'+id , {headers: headers});

    }


}